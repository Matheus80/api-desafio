<?php

namespace App\Modules\MovimentoProduto\Models;

use Illuminate\Database\Eloquent\Model;

class MovimentoProduto extends Model
{
    //
    protected $fillable = ['produto_id', 'quantidade', 'tipo'];
    protected $table = 'movimento_produtos';
}
