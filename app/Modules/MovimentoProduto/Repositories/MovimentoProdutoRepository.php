<?php

namespace App\Modules\MovimentoProduto\Repositories;
use App\Modules\MovimentoProduto\Models\MovimentoProduto;

class MovimentoProdutoRepository {
    
    public function addQtdInicialProduto ($quantidade, $produto_id) {
        $movimento_produto = new MovimentoProduto();
        $movimento_produto->produto_id = $produto_id;
        $movimento_produto->quantidade = $quantidade;
        $movimento_produto->tipo = 'A';
        $movimento_produto->save();
    }

    public function deleteHistoricoMovimentacaoProduto ($produto_id) {
        return MovimentoProduto::where('produto_id', $produto_id)->delete();
    }
}