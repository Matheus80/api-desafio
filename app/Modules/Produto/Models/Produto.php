<?php

namespace App\Modules\Produto\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //
    protected $fillable = ['nome', 'sku'];
    protected $table = 'produtos';
}
