<?php

namespace App\Modules\Produto\Services;
use App\Modules\Produto\Repositories\ProdutoRepository;
use App\Modules\MovimentoProduto\Repositories\MovimentoProdutoRepository;

class ProdutoService {
    private $produto_repository;
    private $movimento_produto_repository;

    public function __construct (ProdutoRepository $produto_repository, MovimentoProdutoRepository $movimento_produto_repository) {
        $this->produto_repository = $produto_repository;
        $this->movimento_produto_repository = $movimento_produto_repository;
    }

    public function listarDados ($id = null) {
        if ($id) {
            $produto = $this->produto_repository->listar($id);
        } else {
            $produto = $this->produto_repository->listar();
        }

        return $produto;
    }

    public function gravarDados ($request, $id = null) {
        if ($id) {
            // Adiciona o produto
            $produto = $this->produto_repository->atualizar($request, $id);
        } else {
            $produto = $this->produto_repository->gravar($request);

            //Adiciona a quantidade inicial do produto
            $movimento_produto = $this->movimento_produto_repository->addQtdInicialProduto($request->quantidade_inicial, $produto->id);
        }

        return $produto;
    }

    public function excluirDados ($id) {
        // Exclui todo o registro de movimentação do produto
        $this->movimento_produto_repository->deleteHistoricoMovimentacaoProduto($id);

        // Exclui os dados referente ao produto
        return $this->produto_repository->excluir($id);
    }
}