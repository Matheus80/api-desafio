<?php

namespace App\Modules\Produto\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Produto\Services\ProdutoService;

class ProdutoController extends Controller
{
    //
    private $produto_service;
    public function __construct(ProdutoService $produto_service) {
        $this->produto_service = $produto_service;
    }
    public function index() {
        try {
            $produtos = $this->produto_service->listarDados();
            return $produtos;
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'success' => false,
                    'message' => 'Ocorreu uma falha ao listar os produtos',
                    'technicalMessage' => $e->getMessage()
                ),
                400
            );
        }
    }
    public function show($id) {
        try {
            $produtos = $this->produto_service->listarDados($id);
            return $produtos;
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'success' => false,
                    'message' => 'Ocorreu uma falha ao listar o produto',
                    'technicalMessage' => $e->getMessage()
                ),
                400
            );
        }
    }
    public function store(Request $request) {
        try {
            $produtos = $this->produto_service->gravarDados($request);
            return $produtos;
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'success' => false,
                    'message' => 'Ocorreu uma falha ao cadastrar o produto',
                    'technicalMessage' => $e->getMessage()
                ),
                400
            );
        }
    }
    public function update($id, Request $request) {
        try {
            $produtos = $this->produto_service->gravarDados($request, $id);
            return $produtos;
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'success' => false,
                    'message' => 'Ocorreu uma falha ao atualizar o produto',
                    'technicalMessage' => $e->getMessage()
                ),
                400
            );
        }
    }
    public function destroy($id) {
        try {
            $produtos = $this->produto_service->excluirDados($id);
            return $produtos;
        } catch (\Exception $e) {
            return response()->json(
                array(
                    'success' => false,
                    'message' => 'Ocorreu uma falha ao excluir o produto',
                    'technicalMessage' => $e->getMessage()
                ),
                400
            );
        }
    }

}
