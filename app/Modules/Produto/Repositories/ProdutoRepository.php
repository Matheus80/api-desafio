<?php

namespace App\Modules\Produto\Repositories;
use App\Modules\Produto\Models\Produto;

class ProdutoRepository {
    public function listar ($id = null) {
        
        $produto = new Produto();
        if ($id) {
            $produto = $produto->where('id', $id)->first();
        } else {
            $produto = $produto->where(function ($query) {
                if (request()->nome) {
                    $query->where('nome', 'like', '%'.request()->nome.'%');
                }

                if (request()->sku) {
                    $query->where('sku', 'like', '%'.request()->sku.'%');
                }

            })->orderBy('nome')->get();
        }

        return $produto;
    }
    
    public function gravar ($request) {
        $produto = new Produto($request->all());
        $produto->save();

        return $produto;
    }
    
    public function atualizar ($request, $id) {
        $produto = Produto::where('id', $id)->update($request->only(['nome', 'sku']));
        return $produto;
    }

    public function excluir ($id) {
        $produto = Produto::where('id', $id)->delete();
        return $produto;
    }
}