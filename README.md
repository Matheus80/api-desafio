Tecnologias utilizadas:
PHP: v7.4.6
Laravel: v8.73.1
Base de dados: Mysql


### Sobre a api ###

Para inicializar a api basta seguir os seguintes passos:

	1° Composer install
	2° Na pasta da api digitar: php artisan serve

# Rotas referente a produtos:

Parâmetros e tipos:

 * nome: string(120), obrigatório
 * sku: string(20), obrigatório
 * quantidade_inicial: double(10,2), obrigatório(POST)

Listagem de todos os produtos

* Tipo: Get
* Rota: /produtos

Lista um produto com ID especifico

Tipo: Get

* Rota: /produtos/{produto_id}

Lista produtos pesquisando por nome e sku

Tipo: Get

* Rota: /produtos
* Parâmetros: nome(opcional), sku(opcional)

Adição de um produto

Tipo: Post

* Rota: /produtos 
* Parâmetros: nome(obrigatório), sku(obrigatório), quantidade_inicial(obrigatório)

Atualizar o nome e sku de um produto

Tipo: Put

* Rota: /produtos/{produto_id}
* Parâmetros: nome(obrigatório), sku(obrigatório)


Excluir um produto

* Tipo: Delete
* Rota: /produtos/{produto_id}

# Rotas referente a movimentação de produtos

Parâmetros e tipos:

 * quantidade: double(10,2), obrigatório
 * tipo: char(1), obrigatório, aceita somente "A" ou "R"
 * produto_id: int, obrigatório

Listagem de todos as movimentações de todos os produtos

* Tipo: Get
* rota /movimento-produtos

Lista uma movimentação específica

* Tipo: Get
* Rota: /movimento-produtos/{movimento_produto_id}

Lista movimentações por tipo, quantidade e produto_id

* Tipo: Get
* Rota: /movimento-produtos
* Parâmetros: quantidade(opcional), tipo(opcional), produto_id(opcional)

Adição de uma movimentação

* Tipo: Post
* Rota: /movimento-produtos
* Parâmetros: quantidade(obrigatório), tipo(obrigatório), produto_id(obrigatório)

Atualizar uma movimentação específica

* Tipo: Put
* Rota: /movimento-produtos/{movimento_produto_id}
* Parâmetros: quantidade(obrigatório), tipo(obrigatório), produto_id(obrigatório)


Excluir uma movimentação específica

* Tipo: Delete
* Rota: /movimento-produtos/{movimento_produto_id}

# Rotas e parâmetros referente ao histórico de movimentação de um produto

Parâmetros e tipos

* sku: string(20), obrigatório

Listagem do histórico de movimentação

* Tipo: Get
* Rota: /historico-movimentacao
* Parâmetros: sku(opcional)

